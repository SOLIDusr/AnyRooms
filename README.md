[![Build contributors](https://img.shields.io/badge/CONTRIBUTORS-2-green)](https://github.com/SOLIDusr/AnyRooms/graphs/contributors)
<img align="left" src="https://github.com/SOLIDusr/AnyRooms/blob/main/logo.png" width="128" height="128"> 

## AnyRooms

SOLID Discord bot for Moderating lobbys and game servers




## Idea

Remake of existing discord bot called "VoiceMaster" (https://voicemaster.xyz/)

## Plans

* Allow users to create private voice channels on your server. 
* Easily manage them without commands.
* Limit channel max users amount and create invites into your channel.
* Finding anyones profile in games like (Fortnite, Valorant, Apex Legends, Destiny 2, Call of duty, R6 and many others).
* I'll add more later.
