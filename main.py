# Importing some libs
import discord
from discord.ext import commands
from selenium import webdriver
import time
import os

# Adding discord intents
discord_intents = discord.Intents.all()
discord_intents.members = True

# Setting up bot profile activity
activity = discord.Activity(name='Dolta Sqad', type=discord.ActivityType.streaming)
# Creating bot class to work with
bot = commands.Bot(intents=discord_intents, command_prefix='!', activity=activity)


# @bot.event - decorator to manage events on server

@bot.event
# Creating async def to check if bot is ready
async def on_ready():
    print('I am ready!')

    # tbh idk wtf that means
    await bot.add_cog(Funcs())


@bot.command()
# command stats to find a Valorant profile on Tracker.gg
async def stats(ctx, amount: str):

    channel = bot.get_channel(1003272756781125752)
    # setting up stats channel as channel

    info = amount.split('#')
    # Saving nickname and tag

    driver = webdriver.Firefox()
    # setting up firefox driver

    name = info[0]
    tag = info[1]

    driver.get("https://tracker.gg/valorant/profile/riot/{}%23{}/overview".format(name, tag))
    # open profile

    time.sleep(10)

    driver.find_element("xpath", '//*[@id="onetrust-accept-btn-handler"]').click()

    # accept cookies

    time.sleep(3)

    print(info)

    a = (driver.find_element('xpath', '//*[@id="app"]/div[2]/div[2]/div/main/div[3]/div[3]/div[3]/div[2]/div[1]/div[2]/div[2]/div/div[1]/div[1]/span[2]').text) # Запоминает ранг
    b = (driver.find_element('xpath', '//*[@id="app"]/div[2]/div[2]/div/main/div[3]/div[3]/div[3]/div[2]/div[1]/div[2]/div[2]/div/div[1]/div[1]/span[1]').text) # Запоминает ранг
    c = (driver.find_element('xpath', '//*[@id="app"]/div[2]/div[2]/div/main/div[3]/div[3]/div[3]/div[2]/div[1]/div[3]/div[2]/div/div[1]/span[2]').text) # Запоминает КД
    d = (driver.find_element('xpath', '//*[@id="app"]/div[2]/div[2]/div/main/div[3]/div[3]/div[3]/div[2]/div[1]/div[3]/div[2]/div/div[1]/span[1]').text) # Запоминает КД

    screen = driver.save_screenshot('screenshot-FireFox.png')
    # Сохраняет скрин статистики

    driver.quit()

    await channel.send("📝Nick:" + ' ' + info[0] + info[1] +'\n' +"📌Rank:" + ' ' + b + ' ' + ":"+ ' ' + a + '\n' + "🎯"+ d + ":" + ' ' + c) # Выводит статистику текстом в дискорд

    with open('screenshot-FireFox.png', 'rb') as f:
        # Открывает скриншот
        picture = discord.File(f)
        await channel.send(file=picture)
        # Выводит скриншот в дискорд
        f.close()
        # Закрывает скриншот
        os.remove('C:\\Users\\User\\Desktop\\bot\\screenshot-FireFox.png')
        # Удаляет скриншот

# Creating class with bot commands called "COG"


class Funcs(commands.Cog):

    # Stng important
    def __init__(self):
        self.bot = bot

    # Command decorator in class
    @commands.command()
    async def rand(self, ctx, *args):
        await ctx.reply("ПНХ")

    @commands.command()
    # Trying to create voice channel
    async def create(self, ctx):
        try:
            await ctx.send('Создаём канал!')
            category = await ctx.guild.create_category("Private Voice", reason=None)
            await ctx.guild.create_voice_channel("[+] Create Voice", category=category, reason=None)
            await ctx.send("Setup finished!")
        except Exception as errors:
            print(f"Bot Error: {errors}")


# Running bot with my token
bot.run("TOKEN")
